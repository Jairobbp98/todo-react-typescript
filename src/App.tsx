import React, { useEffect } from "react";
import Navigation from "./components/Navigation";
import ContainerMain from "./components/Container";
import Row from "./components/Container";
import Tasks from "./components/Tasks";
import M from "materialize-css";
//CSS
import "materialize-css/dist/css/materialize.min.css";
import "./styles/main.sass";

function App(): JSX.Element {
  useEffect(() => {
    M.AutoInit();
  });

  return (
    <ContainerMain className={"container"}>
      <Row className={"row rowmain"}>
        <Navigation />
        <Tasks />
      </Row>
    </ContainerMain>
  );
}

export default App;
