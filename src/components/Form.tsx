import React from "react";
import Container from "./Container";
import RowForm from "./Container";
import ContainerForm from "./Container";
import InputNameTask from "./Form/InputNameTask";
import InputList from "./Form/InputList";
import InputCategory from "./Form/InputCategory";
import InputAddTask from "./Form/InputAddTask";
import InputDescription from "./Form/InputDescription";
import useChangeTask from "../hooks/useChangeTask";

function Form(): JSX.Element {

  const { ChangeText } = useChangeTask();

  return (
    <Container className={"col s12 row containerForm"}>
      <form className={"col s12"}>
        <ContainerForm className={"row mb-0"}>
          <RowForm
            className={"row mb-0"}
            styles={{ marginBottom: "0" }}
          >
            <InputNameTask 
              Event={ChangeText}
            />
          </RowForm>
          <RowForm className={"row mb-0"}>
            <InputList 
              ChangeText={ChangeText}
            />
            <InputCategory 
              ChangeText={ChangeText}
            />
          </RowForm>
          <RowForm className={"row mb-0"}>
            <InputDescription 
              Event={ChangeText}
            />
          </RowForm>
          <RowForm className={"row mb-0"}>
            <InputAddTask />
          </RowForm>
        </ContainerForm>
      </form>
    </Container>
  );
}

export default Form;