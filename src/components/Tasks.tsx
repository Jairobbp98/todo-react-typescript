import React from "react"
import Container from "./Container"
import Todo from "./Tasks/Todo"
import InProgress from "./Tasks/InProgress"
import Done from "./Tasks/Done"

function Tasks(): JSX.Element {
  return(
    <Container className={"col s9 row containerTasks"}>
      <Todo />
      <InProgress />
      <Done />
    </Container>
  )
}

export default Tasks