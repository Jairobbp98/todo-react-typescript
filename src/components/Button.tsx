import React, { Fragment } from "react";
import { IButton } from "../Interfaces/Interfaces";

function Button(props: IButton): JSX.Element {
  return (
    <Fragment>
      <button
        className={props.className}
        style={props.styles}
        onClick={props.Event}
        type={!props.type ? "button" : props.type}
        disabled={props.disabled}
      >
        {props.children}
        {props.Text}
      </button>
    </Fragment>
  );
}

export default Button;
