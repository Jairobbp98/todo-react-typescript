import React, { useState, Fragment } from "react";
import { IDropDownButton } from '../../Interfaces/Interfaces'

function ListOptions(Value: String, Index: number): JSX.Element {
  return Index === 0 ? (
    <option key={Index} value="" disabled>
      {Value}
    </option>
  ) : (
    <option key={Index} value={Index}>
      {Value}
    </option>
  );
}

function DropDownButton(props: IDropDownButton): JSX.Element {
  const [opciones] = useState<string[]>(props.Options);

  return (
    <Fragment>
      <select 
        defaultValue=""
        onChange={props.Event}
        id={props.id}>
        {opciones.map((value, index) => ListOptions(value, index))}
      </select>
      <label className={"" + props.classLabel}>
        {props.children}
        {props.Title}
      </label>
    </Fragment>
  );
}

export default DropDownButton;
