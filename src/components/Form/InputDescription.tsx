import React, { useEffect }  from "react"
import Container from "../Container"
import { EventInput } from "../../Interfaces/Interfaces";
import M from "materialize-css";

function InputDescription(props: EventInput): JSX.Element {

  useEffect(() => {
    const inputTextAreaId = document.querySelector('#description')
    new M.CharacterCounter(inputTextAreaId as Element);
  }, [])

  return (
    <Container 
      className={"input-field col s12"}>
      <textarea 
        id="description" 
        className="materialize-textarea"
        placeholder="Add to description for the task"
        maxLength={200}
        data-length={200} 
        onChange={props.Event}/>
      <label htmlFor="description">
        Description
      </label>
    </Container>
  )
}

export default InputDescription;