import React from "react";
import AddTask from "../Button";
import Container from "../Container";
import { Add as AddIcon } from "@material-ui/icons";
import { useDispatch } from "react-redux";
import handleAddTask from "../../hooks/addTask";

function InputAddTask(): JSX.Element {
  const dispatch = useDispatch();

  return (
    <Container
      className={"col s12 addTask"}
    >
      <AddTask
        Text="ADD TASK"
        className="waves-effect waves-light btn"
        Event={(e) => handleAddTask(e, dispatch)}
      >
        <AddIcon />
      </AddTask>
    </Container>
  );
}

export default InputAddTask;
