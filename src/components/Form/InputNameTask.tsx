import React, { useEffect, useState } from "react";
import Container from "../Container";
import { AssignmentTurnedIn as TaskIcon } from '@material-ui/icons'
import CSS from "csstype"
import { EventInput } from "../../Interfaces/Interfaces";


function InputNameTask(props: EventInput): JSX.Element {
  useEffect(() => {
    const inputTextAreaId = document.querySelector('#nameTask')
    new M.CharacterCounter(inputTextAreaId as Element);
  }, [])

  const [StylesLabel] = useState<CSS.Properties>({
    display: "flex",
    alignItems: "center",
    textAlign: "center"
  })

  const [StyleContainer] = useState<CSS.Properties>({
    marginTop: "7%"
  })

  return (
    <Container className={"input-field col s12"} styles={StyleContainer}>
      <input 
        placeholder="Add name task"
        id="nameTask"
        type="text"
        maxLength={50}
        data-length={50}
        onChange={props.Event}
      />
      <label htmlFor="nameTask" style={StylesLabel}>
        <TaskIcon />
        Name Task
      </label>
    </Container>
  )
}

export default InputNameTask;