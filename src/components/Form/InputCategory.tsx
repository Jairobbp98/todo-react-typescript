import React, { useState } from "react";
import Container from "../Container";
import DropDownButton from "./DropDownButton";
import { List as ListIcon } from "@material-ui/icons";
import { DropDownElement, InputElement, TextAreaElement } from "../../Interfaces/Interfaces";

interface InCategory {
  ChangeText: (e: InputElement | DropDownElement | TextAreaElement) => void
}

function InputCategory(props: InCategory): JSX.Element {

  const [Options] = useState<string[]>(
    ["Select List", "Opcion 1", "Opcion 2", "Opcion 3"]
  )

  return (
    <Container className={"input-field col s6"}>
      <DropDownButton
        Title="Category"
        Options={Options}
        classLabel="TitleList"
        id="category"
        Event={props.ChangeText}
      >
        <ListIcon />
      </DropDownButton>
    </Container>
  );
}

export default InputCategory;