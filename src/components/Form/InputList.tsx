import React, { useState } from "react";
import Container from "../Container";
import DropDownButton from "./DropDownButton";
import { List as ListIcon } from "@material-ui/icons";
import { DropDownElement, InputElement, TextAreaElement } from "../../Interfaces/Interfaces";

interface InList {
  ChangeText: (e: InputElement | DropDownElement | TextAreaElement) => void
}

function InputList(props: InList): JSX.Element {

  const [Options] = useState<string[]>(
    ["Select List", "Opcion 1", "Opcion 2", "Opcion 3"]
  )

  return (
    <Container className={"input-field col s6"}>
      <DropDownButton
        Title="List"
        Options={Options}
        classLabel="TitleList"
        id="nameList"
        Event={props.ChangeText}
      >
        <ListIcon />
      </DropDownButton>
    </Container>
  );
}

export default InputList;