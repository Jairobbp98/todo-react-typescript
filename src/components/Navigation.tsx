import React  from "react";
import NavigationHeader from "./Navigation/NavigationHeader";
import NavigationBody from "./Navigation/NavigationBody";
import NavigationFooter from "./Navigation/NavigationFooter";

const Navigation = (): JSX.Element => {
  return (
      <header className="col s3 row">
        <NavigationHeader />
        <NavigationBody />
        <NavigationFooter />
      </header>
  );
};

export default Navigation;
