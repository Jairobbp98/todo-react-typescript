import React from "react"
import Container from "../Container"
import TitleTodo from "../Container"
import { AppState } from "../../store/rootStore"
import { useSelector } from "react-redux"
import CardTask from "./CardTask"
import { IList } from "../../Interfaces/Interfaces"

function Todo() {
  const tareas = useSelector((state: AppState) => state.tasks.tasks);

  return (
    <Container className={"containerTodo col s4 row"}>
      <TitleTodo className={"titletodo col s12 center-align"}>
        <h2 className="white-text">TODO</h2>
      </TitleTodo>
      <>
        {
          tareas.map((element: IList) =>(
            element.status === "default" ?
            <CardTask value={element} key={element.index} /> :
            null
          ))
        }
      </>
    </Container>
  )
}

export default Todo;