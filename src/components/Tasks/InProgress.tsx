import React from "react"
import { useSelector } from "react-redux";
import { IList } from "../../Interfaces/Interfaces";
import { AppState } from "../../store/rootStore";
import Container from "../Container"
import TitleTodo from "../Container"
import CardTask from "./CardTask";

function InProgress() {
  const tareas = useSelector((state: AppState) => state.tasks.tasks);

  return (
    <Container className={"containerProgress col s4 row"}>
      <TitleTodo className={"titleprogress col s12 center-align"}>
        <h2 className="white-text">IN-PROGRESS</h2>
      </TitleTodo>
      <>
        {
          tareas.map((element: IList) => (
            element.status === "in-progress" ?
            <CardTask value={element} key={element.index} /> :
            null
          ))
        }
      </>
    </Container>
  )
}

export default InProgress;