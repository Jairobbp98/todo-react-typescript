import React from "react";
import ContainerCard from "../Container";
import RowInformation from "../Container";
import CardHeader from "./CardTask/CardHeader";
import CardBody from "./CardTask/CardBody";
import CardFooter from "./CardTask/CardFooter";
import { IList } from "../../Interfaces/Interfaces";

type Props = {
  value: IList;
};

function CardTask(props: Props) {
  const { value } = props;

  return (
    <ContainerCard className={"containerCard col s12 row"} key={value.index}>
      <RowInformation className={"rowInformation col s12 card white"}>
        <CardHeader
          date={value.date}
          time={value.time}
          nameTask={value.nameTask}
          status={value.status}
        />
        <CardBody description={value.description} />
        <CardFooter value={value} />
      </RowInformation>
    </ContainerCard>
  );
}

export default CardTask;
