import React from "react";
import ContainerCardFooter from "../../Container";
import OkButton from "../../Button";
import DeleteButton from "../../Button";
import { DeleteOutline as DeleteIcon } from "@material-ui/icons";
import { ArrowForwardIos as ArrowIcon } from "@material-ui/icons";
import { useDispatch } from "react-redux";
import {
  ActionDeleteTask,
  ActionUpdateCategoryTask,
} from "../../../store/models/Tasks/TaskAction";
import { IList } from "../../../Interfaces/Interfaces";
import swal from "sweetalert";

type ICardFooter = {
  value: IList;
};

function CardFooter(props: ICardFooter) {
  const dispatch = useDispatch();

  const statusCard = (state: string) => {
    switch (state) {
      case "default":
        return "in-progress";
      case "in-progress":
        return "done";
      case "done":
        return "true";
      default:
        return "default";
    }
  };

  const handleDeleteTask = () => {
    dispatch(ActionDeleteTask(props.value));
    swal({
      icon: "success",
      title: "Tarea Eliminada",
    });
  };

  const handleUpdateCategoryTask = () => {
    dispatch(
      ActionUpdateCategoryTask({
        ...props.value,
        status: statusCard(props.value.status),
      })
    );

    swal({
      icon: "success",
      title: "Tarea en Progreso",
    });
  };

  const classColorButton = () => {
    if (props.value.status === "default") {
      return "#1C2846";
    } else if (props.value.status === "in-progress") {
      return "#E2573B";
    } else {
      return "#315052";
    }
  };

  return (
    <ContainerCardFooter className={"cardFooter"}>
      <DeleteButton
        className={"btn waves-effect waves-light"}
        Event={handleDeleteTask}
        type={"button"}
        styles={{
          backgroundColor: classColorButton(),
        }}
      >
        <DeleteIcon />
      </DeleteButton>
      <span>{props.value.category}</span>
      <OkButton
        className={`btn ${props.value.status}`}
        Event={handleUpdateCategoryTask}
        styles={{
          backgroundColor: classColorButton(),
        }}
        disabled={statusCard(props.value.status) === "true" ? true : false}
      >
        <ArrowIcon />
      </OkButton>
    </ContainerCardFooter>
  );
}

export default CardFooter;
