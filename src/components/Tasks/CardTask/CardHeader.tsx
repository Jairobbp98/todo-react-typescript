import React from "react";
import ContainerCardTitle from "../../Container";
import ContainerFecha from "../../Container";
import SeparetionLine from "../../Container";

interface ICardHeader {
  date: string
  time: string
  nameTask: string
  status: string
}

function CardHeader({ date, time, nameTask, status }: ICardHeader) {
  const ColorButtons = () => {
    if (status === "default") {
      return "#1C2846"
    } else if (status === "in-progress"){
      return "#E2573B"
    }
  }

  return (
    <ContainerCardTitle className={"card-content"}>
      <span className={"card-title"}>{ nameTask }</span>
      <ContainerFecha className={"containerFecha"}>
        <span>
          { time } <br />
          { date }
        </span>
      </ContainerFecha>
      <SeparetionLine className={"SeparetionLine"} styles={{
        backgroundColor: ColorButtons()
      }}/>
    </ContainerCardTitle>
  );
}

export default CardHeader;