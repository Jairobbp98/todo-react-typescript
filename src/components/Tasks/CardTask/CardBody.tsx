import React from "react";
import ContainerCardDescription from "../../Container";

interface ICardBody{
  description: string
}

function CardBody(props: ICardBody) {
  return (
    <ContainerCardDescription className={"cardDescription"}>
      <p>
        {
          props.description
        }
      </p>
    </ContainerCardDescription>
  );
}

export default CardBody;
