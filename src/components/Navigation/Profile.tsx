import React from "react";

function Profile(): JSX.Element {
  return (
    <figure className="profile">
      <img className="responsive-img circle" src="https://placeimg.com/100/100/people" alt="imagen de perdil" />
    </figure>
  );
}

export default Profile;
