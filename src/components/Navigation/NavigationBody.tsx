import React from "react";
import Profile from "./Profile";
import ContainerNavigationBody from "../Container";
import Tabs from "./Tabs";
import FormTask from '../Form'

function NavigationBody(): JSX.Element {
  return (
    <ContainerNavigationBody className={"navigationBody col s12 row"}>
      <Profile />
      <Tabs />
      <FormTask />
    </ContainerNavigationBody>
  );
}

export default NavigationBody;
