import React, { useState, useEffect } from "react";
import ContainerDate from "../Container";

function Fecha() {
  const [day, setDay] = useState<string>("00");
  const [month, setMonth] = useState<string>("00");
  const [year, setYear] = useState<string>("1998");

  const Dates = () => {
    const fecha = new Date();
    const day: string =
      fecha.getDate() <= 9 ? `0${fecha.getDate()}` : `${fecha.getDate()}`;
    const month: string =
      (fecha.getMonth() <= 8)
        ? `0${fecha.getMonth() + 1}`
        : `${fecha.getMonth() + 1}`;
    const year: string = fecha.getFullYear().toString();

    setDay(day);
    setMonth(month);
    setYear(year);
  };

  useEffect(() => {
    Dates();
  });

  return (
    <ContainerDate className={"fecha col s6 center-align"}>
      <h2>
        {day}/{month}/{year}
      </h2>
    </ContainerDate>
  );
}

export default Fecha;
