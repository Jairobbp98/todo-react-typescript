import React, { useState } from "react";
import ContainerTabs from "../Container";
import NavContent from "../Container";

function Tabs() {
  const [indicator, setIndicator] = useState({
    taskIndicator: true,
    listIndicator: false
  });

  const toggleClass = () => {
    setIndicator({
      taskIndicator: !indicator.taskIndicator,
      listIndicator: !indicator.listIndicator
    })
  }

  return (
    <ContainerTabs className={"row containerTabs col s12"}>
        <NavContent className={"col s12"}>
          <ul className="tabs tabs-transparent tabs-fixed-width">
            <li className="tab">
              <a 
                className={indicator.taskIndicator ? "active" : ""} 
                href="#test1" 
                onClick={toggleClass}>
                  Tasks
              </a>
            </li>
            <li className="tab">
              <a 
                className={indicator.listIndicator ? "active" : ""} 
                href="#test2" 
                onClick={toggleClass}>
                  Lists
              </a>
            </li>
          </ul>
        </NavContent>
    </ContainerTabs>
  );
}

export default Tabs;
