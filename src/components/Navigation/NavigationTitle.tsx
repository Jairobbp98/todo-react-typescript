import React, { useState, useEffect } from "react";
import ContainerTitle from "../Container";

function Titulo(props: { titulo: string }): JSX.Element {
  const [title, setTitle] = useState<string>(props.titulo);

  useEffect(() => {
    setTitle(props.titulo ? props.titulo : "Welcome");
  }, [props.titulo]);

  return (
    <ContainerTitle className={"bienvenida col s12 center-align"}>
      <h2>{title ? title : "Welcome"}</h2>
    </ContainerTitle>
  );
}

export default Titulo;
