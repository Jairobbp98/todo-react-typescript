import React from "react"
import ContainerNavigationFooter from '../Container'
import RowImg from "../Container"
import RowText from "../Container"
import { ReactComponent as GitLabImg } from "../../img/icons/gitlab.svg"

function NavigationFooter(): JSX.Element {
  return(
    <ContainerNavigationFooter className={"col s12 row NavigationFooter"}>
      <RowImg className={"col s12 imgFooter"}>
        <GitLabImg 
          width="8em"
          height="8em"/>
      </RowImg>
      <RowText className={"col s12 textFooter"}>
        <span>Development by Jairobbp98</span>
      </RowText>
    </ContainerNavigationFooter>
  )
}

export default NavigationFooter;