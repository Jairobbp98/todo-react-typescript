import React, { useState } from "react";
import CSS from 'csstype'
import Reloj from "./Reloj";
import Fecha from "./Fecha";
import Title from "./NavigationTitle";
import ContainerHeaderNavigation from "../Container";

function NavigationHeader(): JSX.Element {

  const [colorBackground] = useState<CSS.Properties>({
    backgroundColor: "#1C2846"
  });

  return (
    <ContainerHeaderNavigation className={"col s12 navigationHeader"} styles={colorBackground}>
      <Reloj />
      <Fecha />
      <Title titulo="Bienvenido" />
    </ContainerHeaderNavigation>
  );
}

export default NavigationHeader;
