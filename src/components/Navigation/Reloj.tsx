import React, { useState, useEffect } from "react";
import ContainerReloj from "../Container";

function Reloj(): JSX.Element {
  let timer: NodeJS.Timer;
  const [horas, setHoras] = useState<number>();
  const [minutos, setMinutos] = useState<string>();
  const [segundos, setSegundos] = useState<string>();
  
  const reloj = () => {
    timer = setInterval(() => {
      let tiempo = new Date();
      if (horas !== tiempo.getHours()) setHoras(tiempo.getHours());

      if (minutos !== `${tiempo.getMinutes()}`)
        setMinutos(
          tiempo.getMinutes() <= 9
            ? `0${tiempo.getMinutes()}`
            : `${tiempo.getMinutes()}`
        );
      setSegundos(
        tiempo.getSeconds() <= 9
          ? `0${tiempo.getSeconds()}`
          : `${tiempo.getSeconds()}`
      );
    }, 1000);
  };

  useEffect(() => {
    reloj();
    return () => {
      clearInterval(timer);
    };
  });

  return (
    <ContainerReloj className={"reloj col s6 center-align"}>
      <h2>
        {horas} : {minutos} : {segundos}
      </h2>
    </ContainerReloj>
  );
}

export default Reloj;
