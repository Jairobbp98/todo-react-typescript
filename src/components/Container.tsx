import React  from "react";
import { IContainer } from '../Interfaces/Interfaces'

function Container(props: IContainer){
  return (
    <div className={props.className} style={props.styles}>
      {props.children}
    </div>
  );
}

export default Container;
