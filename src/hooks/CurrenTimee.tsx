import { IHora } from '../Interfaces/Interfaces';

function CurrenTimee(tiempo: Date): IHora {
  const horas = tiempo.getHours();
  const minutos = tiempo.getMinutes() <= 9 ? `0${tiempo.getMinutes()}` : `${tiempo.getMinutes()}`;
  const segundos = tiempo.getSeconds() <= 9 ? `0${tiempo.getSeconds()}` : `${tiempo.getSeconds()}`;
  return {
    horas,
    minutos,
    segundos
  }
}

export default CurrenTimee;