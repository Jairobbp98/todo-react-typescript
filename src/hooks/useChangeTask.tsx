import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from "react-redux";
import { IList, InputElement, DropDownElement, TextAreaElement, ButtonElement } from '../Interfaces/Interfaces'
import { ActionNewTask } from '../store/models/Tasks/TaskAction';
import { AppState } from '../store/rootStore';

function useChangeTask() {
  const [list, setList] = useState<IList | {}>({});
  const dispatch = useDispatch();
  const tareas = useSelector((state: AppState) => state.tasks);

  useEffect(() => {
    dispatch(ActionNewTask(list as IList));
  }, [dispatch, list])

  const ChangeText = (e: ButtonElement | InputElement | DropDownElement | TextAreaElement) => {
    setList({
      ...list,
      index: tareas.tasks.length === 0 ? 1 : tareas.tasks.length + 1,
      [e.currentTarget.id]: e.currentTarget.value
    })
  }

  return { list, ChangeText }
}

export default useChangeTask