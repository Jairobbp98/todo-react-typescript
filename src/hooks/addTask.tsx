import { Dispatch } from "react";
import { EventButton } from "../Interfaces/Interfaces";
import { ActionAddTask, ActionNewTask } from "../store/models/Tasks/TaskAction";
import CurrentDate from "./CurrentDate";
import CurrenTimee from "./CurrenTimee";
import swal from "sweetalert"

function handleAddTask(e: EventButton, dispatch: Dispatch<any>) {
  e.preventDefault();
  const tiempo = new Date();
  const theHour = CurrenTimee(tiempo);
  const theDate = CurrentDate(tiempo);
  dispatch(
    ActionNewTask({
      time: `${theHour.horas}:${theHour.minutos}:${theHour.segundos}`,
      date: `${theDate.day}-${theDate.month}-${theDate.year}`,
      status: "TODO",
    })
  );
  dispatch(ActionAddTask());
  swal({
    icon: "success",
    title: "Tarea Agregada"
  })
}

export default handleAddTask;
