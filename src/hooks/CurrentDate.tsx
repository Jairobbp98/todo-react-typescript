import { IFecha } from "../Interfaces/Interfaces";

function CurrentDate(tiempo: Date): IFecha {
  const day: string =
    tiempo.getDate() <= 9 ? `0${tiempo.getDate()}` : `${tiempo.getDate()}`;
  const month: string =
    tiempo.getMonth() <= 9
      ? `0${tiempo.getMonth() + 1}`
      : `${tiempo.getMonth() + 1}`;
  const year: string = tiempo.getFullYear().toString();

  return {
    day,
    month,
    year,
  };
}

export default CurrentDate;
