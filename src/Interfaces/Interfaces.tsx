import CSS from "csstype";

//FormsTypes
type ButtonElement = React.FormEvent<HTMLButtonElement>;
type InputElement = React.FormEvent<HTMLInputElement>;
type EventButton = React.FormEvent<HTMLButtonElement>;
type DropDownElement = React.FormEvent<HTMLSelectElement>;
type TextAreaElement = React.FormEvent<HTMLTextAreaElement>;

interface EventInput {
  Event: (
    e: ButtonElement | InputElement | DropDownElement | TextAreaElement
  ) => void;
}

interface IContainer {
  children?: JSX.Element | JSX.Element[];
  className?: string;
  styles?: CSS.Properties;
}

interface IButton {
  Text?: string;
  className?: string;
  styles?: CSS.Properties;
  Event: (e: ButtonElement) => void;
  children?: JSX.Element;
  type?: "button" | "submit" | "reset";
  disabled?: boolean;
}

interface IDropDownButton {
  Title: String;
  Options: string[];
  children?: JSX.Element;
  classLabel?: String;
  id: string;
  Event?: (e: React.ChangeEvent<HTMLSelectElement>) => void;
}

interface IHora {
  horas: number;
  minutos: string;
  segundos: string;
}

interface IFecha {
  day: string;
  month: string;
  year: string;
}

interface IDates {
  fecha: IFecha;
  hora: IHora;
}

//Redux -- State
interface IList {
  index: number;
  nameTask: string;
  nameList: string;
  category: string;
  description: string;
  time: string;
  date: string;
  status: string;
}

type TasksState = {
  newTask: IList | {} | undefined;
  tasks: IList[];
};

type TasksAction = {
  type: "ADD_TASK" | "DELETE_TASK" | "NEW_TASK" | "UPDATE_TASK";
  Tasks?: IList | {};
};

interface ApplicationsReducers {
  tasks: TasksState;
}

type DispatchType = (args: TasksAction) => TasksAction;

/* TasksCards */

export type {
  EventInput,
  ButtonElement,
  TextAreaElement,
  EventButton,
  InputElement,
  DropDownElement,
  IDropDownButton,
  IButton,
  IContainer,
  IList,
  TasksState,
  TasksAction,
  DispatchType,
  ApplicationsReducers,
  IDates,
  IHora,
  IFecha,
};
