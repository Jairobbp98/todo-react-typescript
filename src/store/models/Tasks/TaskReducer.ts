import { NEW_TASK, ADD_TASK, DELETE_TASK, UPDATE_TASK } from "./models/actions";
import { TasksState, TasksAction, IList } from '../../../Interfaces/Interfaces'

const defaultState: TasksState = {
  newTask: {},
  tasks: [
    {
      index: 1,
      nameTask: "tareaPrueba",
      nameList: "default",
      category: "default",
      description: "lorem ipsum",
      time: "00:00",
      date: "02-06-1998",
      status: "default",
    },
    {
      index: 2,
      nameTask: "tareaPrueba",
      nameList: "default",
      category: "default",
      description: "lorem ipsum",
      time: "00:00",
      date: "02-06-1998",
      status: "default",
    },
  ]
};

function TaskReducer(
  state: TasksState = defaultState,
  action: TasksAction
): TasksState {
  switch (action.type) {
    case NEW_TASK:
      return {
        ...state,
        newTask: {
          ...state.newTask,
          ...action.Tasks
        }
      }
    case ADD_TASK:
      return {
        ...state,
        tasks: [...state.tasks, state.newTask as IList],
        newTask: {}
      }
    case UPDATE_TASK:
      const updateTask = action.Tasks as IList;
      return {
        ...state,
        tasks: state.tasks.map((task) => (
          task.index === updateTask.index ?
          {
            ...task,
            status: updateTask.status
          } :
          {
            ...task
          } 
        ), [])
      }
    case DELETE_TASK:
      const Tasks = action.Tasks as IList;
      const updatingTask: IList[] = state.tasks.filter(
        task => task.index !== Tasks.index
      )
      return {
        ...state,
        tasks: updatingTask
      }
    default:
      return state;
  }
}

export default TaskReducer;