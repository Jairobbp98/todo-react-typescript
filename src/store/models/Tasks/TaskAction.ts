import { NEW_TASK, ADD_TASK, DELETE_TASK, UPDATE_TASK} from './models/actions'
import { IList, DispatchType } from '../../../Interfaces/Interfaces'

export const ActionNewTask = (Tasks: IList | {}) => {
  return (dispatch: DispatchType) => {
    dispatch({
      type: NEW_TASK,
      Tasks
    })
  }
}

export const ActionAddTask = () => {
  return (dispatch: DispatchType) => {
    dispatch({
      type: ADD_TASK,
    })
  }
}

export const ActionUpdateCategoryTask = (Tasks: IList) => {
  return (dispatch: DispatchType) => {
    dispatch({
      type: UPDATE_TASK,
      Tasks
    })
  }
}

export const ActionDeleteTask = (Tasks: IList) => {
  return (dispatch: DispatchType) => {
    dispatch({
      type: DELETE_TASK,
      Tasks
    })
  }
}