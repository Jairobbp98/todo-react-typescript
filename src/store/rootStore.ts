import { createStore, applyMiddleware, Store, combineReducers} from "redux";
import { composeWithDevTools } from 'redux-devtools-extension'
import { DispatchType, ApplicationsReducers } from "../Interfaces/Interfaces";
import TaskReducer from "./models/Tasks/TaskReducer";
import thunk from "redux-thunk";

const rootReducers = combineReducers<ApplicationsReducers>({
  tasks: TaskReducer
})

const TaskStore: Store<ApplicationsReducers> & {
  dispatch: DispatchType;
} = createStore(rootReducers, composeWithDevTools(applyMiddleware(thunk)));

export type AppState = ReturnType<typeof rootReducers>

export default TaskStore;
